<?php

namespace Minicursos;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        'name',
        'description',
        'speaker',
        'start',
        'end',
        'local',
        'max',
        'image',
    ];

    protected $dates = [
        'start',
        'end',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function saveCourse(array $data)
    {
        $data['start'] = Carbon::createFromFormat('Y-m-d H:i', "{$data['date']} {$data['start']}");
        $data['end']   = Carbon::createFromFormat('Y-m-d H:i', "{$data['date']} {$data['end']}");

        $this->fill($data);

        return $this->save();
    }
}
