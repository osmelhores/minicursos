<?php

namespace Minicursos\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminFormRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|min:3|max:255',
            'description' => 'required|string|min:3',
            'speaker' => 'required|string|min:3|max:255',
            'date' => 'required|date',
            'start' => 'required|string',
            'end' => 'required|string',
            'local' => 'required|string',
            'max' => 'required|numeric',
            'image' => 'image',
        ];
    }
}
