<?php

namespace Minicursos\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Minicursos\Course;
use Minicursos\User;

class CertificateController extends Controller
{
    public function show($id)
    {
        $course = Course::find($id);

        if (!$course) {
            return back();
        }

        $users = $course->users()->get();

        return view('admin.certificates.presences')->with(compact('course', 'users'));
    }

    public function generateCertificates(Request $request)
    {
        $courses = Course::where('start', '<', Carbon::now())->get();

        return view('admin.courses.index')->with(compact('courses'));
    }

    public function allCertificates($id)
    {
        $user = User::find($id);

        if (!$user) {
            return back();
        }

        $courses = $user->courses()->where('start', '<', Carbon::now())->get();;

        return view('courses.certificate.list')->with(compact('user', 'courses'));
    }

    public function view(Request $request)
    {
        $user = User::find($request->user_id);

        if (!$user) {
            return back();
        }

        $course = $user->courses->filter(function (Course $course) use ($request) {
            return $course->id = $request->course_id;
        })->first();

        if (!$course) {
            return back();
        }

        $ch = (new Carbon($course->start))->diffInHours($course->end);

        return view('courses.certificate.show')->with(compact('user', 'course', 'ch'));
    }
}
