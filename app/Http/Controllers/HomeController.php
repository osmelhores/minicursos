<?php

namespace Minicursos\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Minicursos\Course;

class HomeController extends Controller
{
    public function index()
    {
        $courses = Course::where('start', '>', Carbon::now())->get();

        return view('courses.index')->with(compact('courses'));
    }
}
