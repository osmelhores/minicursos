<?php

namespace Minicursos\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Minicursos\Course;
use Minicursos\User;
use Minicursos\Http\Requests\AdminFormRequest;
use Minicursos\Repositories\ImageRepository;

class AdminController extends Controller
{
    /**
     * @var \Minicursos\Repoitories\ImageRepository
     */
    private $imageRepo;

    public function __construct(ImageRepository $imageRepo)
    {
        $this->imageRepo = $imageRepo;
    }

    public function index()
    {
        $courses = Course::where('start', '>', Carbon::now())->get();

        return view('admin.courses.index')->with(compact('courses'));
    }

    public function show(int $id)
    {
        $course = Course::find($id);

        return view('admin.courses.show')->with(compact('course'));
    }

    public function create()
    {
        return view('admin.courses.create');
    }

    public function store(AdminFormRequest $request)
    {
        $course = new Course();

        $image = [$this->uploadedImage($request) ?? null];

        if (!$course->saveCourse(array_merge($request->except('image'), $image))) {
            return back();
        }

        return redirect()->route('admin.courses.index');
    }

    public function edit($id)
    {
        $course = Course::find($id);

        if (!$course) {
            return back();
        }

        $start = Carbon::parse($course->start);
        $end   = Carbon::parse($course->end);

        $date  = sprintf('%04d-%02d-%02d', $start->year, $start->month, $start->day);
        $start = sprintf('%02d:%02d', $start->hour, $start->minute);
        $end   = sprintf('%02d:%02d', $end->hour, $end->minute);

        return view('admin.courses.edit')->with(compact('course', 'date', 'start', 'end'));
    }

    public function update(AdminFormRequest $request, $id)
    {
        $course = Course::find($id);

        if (!$course) {
            return back();
        }

        $image = $this->uploadedImage($request) ?? $course->image ?? null;

        if (!$course->saveCourse(array_merge($request->except('image'), compact('image')))) {
            return back();
        }

        return redirect()->route('admin.courses.index');
    }

    public function destroy($id)
    {
        $course = Course::find($id);

        if (!$course) {
            return back();
        }

        $course->users()->get()->map(function (User $user) use ($id) {
            return $user->courses()->detach($id);
        });

        if (!$course->delete()) {
            return back();
        }

        return redirect()->route('admin.courses.index');
    }

    public function previousCourses()
    {
        $now = Carbon::now();

        $courses = Course::where('start', '<', Carbon::now())->get();

        return view('admin.courses.previous')->with(compact('courses'));
    }

    private function uploadedImage(Request $request)
    {
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $image = $this->imageRepo->saveImage($request->image, 'courses', $request->name);
        }

        return $image ?? null;
    }
}
