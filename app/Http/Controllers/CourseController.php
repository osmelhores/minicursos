<?php

namespace Minicursos\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Minicursos\Course;

class CourseController extends Controller
{
    public function index()
    {
        $now = Carbon::now();

        $courses = Course::where('start', '>', Carbon::now())->get();

        return view('courses.index')->with(compact('courses'));
    }

    public function show($id)
    {
        $course = Course::find($id);
        $user = auth()->user() ?? null;

        return view('courses.show')->with(compact('course', 'user'));
    }

    public function subscribed()
    {
        $courses = auth()->user()->courses()->where('start', '>', Carbon::now())->get();

        return view('courses.subscribed')->with(compact('courses'));
    }

    public function subscribe(Request $request)
    {
        auth()->user()->courses()->attach($request->course_id);

        return redirect()->route('courses.subscribed');
    }

    public function unsubscribe(Request $request)
    {
        auth()->user()->courses()->detach($request->id);

        return redirect()->route('courses.index');
    }
}
