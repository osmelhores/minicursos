<?php

declare(strict_types=1);

namespace Minicursos\Repositories;

class ImageRepository
{
    public function saveImage($image, string $folder, ?string $name = null)
    {
        if (!is_null($image)) {
            $extension = $image->extension();

            $filename = sprintf('%s.%s', kebab_case($name ?? time()), $extension);

            $image->storeAs($folder, $filename);

            return $filename;
        };

        return false;
    }
}
