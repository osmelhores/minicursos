@if(!auth()->user() || auth()->user()->admin == false)
  <a href="{{ route('courses.subscribed') }}" class="btn btn-primary">Meus minicursos</a>
@else
  <a href="{{ route('admin.courses.index') }}" class="btn btn-secondary">Área Administrativa</a>
@endif
