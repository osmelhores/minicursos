@foreach($courses as $course)
  <div class="card border-primary mb-3">
    <div class="card-header"><a href="{{ route('courses.show', $course->id) }}">{{ $course->name }}</a></div>
    <img style="height: 150px; width: 150px; border: solid; float: left;" src="{{ url('storage/courses/'.$course->image) }}" alt="{{ $course->name }}">
    <p>Horário: {{ $course->start }} - {{ $course->end }}</p>
    <p>Local: {{ $course->local }}</p>
    <p>Palestrante: {{ $course->speaker }}</p>
  </div>
@endforeach
