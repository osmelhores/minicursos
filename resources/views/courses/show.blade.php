@extends('layouts.app')

@section('content')
<div class="container">
  @include('courses.partials.header-links')
  <h1>{{ $course->name }}</h1>
  <img style="height: 200px; width: 200px; border: solid; float: left;" src="{{ url('storage/courses/'.$course->image) }}" alt="{{ $course->name }}">
  <p>Horário: {{ $course->start }} - {{ $course->end }}</p>
  <p>Local: {{ $course->local }}</p>
  <p>Palestrante: {{ $course->speaker }}</p>
  <p>{{ $course->description }}</p>

  @if(auth()->user() === null)
    <a href="{{ route('login') }}" class="btn btn-primary">Entre no sistema</a> ou <a href="{{ route('register') }}">Registre-se</a>
  @elseif(auth()->user()->courses->find($course->id))
    <form action="{{ route('courses.unsubscribe') }}" method="POST">
      @csrf
      <input type="hidden" name="course_id" value="{{ $course->id }}">
      <button type="submit" class="btn btn-danger">Desinscrever-se</button>
    </form>
  @elseif(!auth()->user()->admin)
    <form action="{{ route('courses.subscribe') }}" method="POST">
      @csrf
      <input type="hidden" name="course_id" value="{{ $course->id }}">
      <button type="submit" class="btn btn-primary">Inscrever-se</button>
    </form>
  @else
    <a href="{{ route('admin.courses.edit', $course->id) }}" class="btn btn-warning">Editar</a>
  @endif
</div>
@endsection
