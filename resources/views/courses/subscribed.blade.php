@extends('layouts.app')

@section('content')
<div class="container">
  <h1>Meus Minicursos</h1>
  @if(!auth()->user() || auth()->user()->admin == false)
    <a href="{{ route('courses.index') }}" class="btn btn-primary">Minicursos disponíveis</a>
    <a href="{{ route('courses.certificates', auth()->user()) }}" class="btn btn-primary">Meus certificados</a>
  @else
    <a href="{{ route('admin.courses.index') }}" class="btn btn-primary">Área Administrativa</a>
  @endif
  <div class="row justify-content-center">
    <div class="col-md-8">
      @include('courses.partials.list')
    </div>
  </div>
</div>
@endsection
