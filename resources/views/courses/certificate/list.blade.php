@extends('layouts.app')

@section('content')
<div class="container">
  <h1>Meus certificados</h1>
  @include('courses.partials.header-links')
  <table class="table table-bordered table-striped">
    <thead>
      <th>Nome</th>
      <th>opções</th>
    </thead>
    <tbody>
      @foreach($courses as $course)
        <tr>
          <td>{{ $course->name }}</td>
          <td width="1%" nowrap>
            <form action="{{ route('courses.certificate') }}" method="POST">
              @csrf
              <input type="hidden" name="user_id" value="{{ $user->id }}">
              <input type="hidden" name="course_id" value="{{ $course->id }}">
              <button type="submit" class="btn btn-sm btn-primary">Visualizar</button>
            </form>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
@endsection
