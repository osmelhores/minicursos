@extends('layouts.app')

@section('content')
<div class="container">
  @include('courses.partials.header-links')
  <h1>{{ $course->name }}</h1>

  <p>Certificamos que {{ $user->name }} participou do minicurso intitulado: "{{ $course->name }}" com carga horária {{ $ch }}</p>
  <p>Realizado no dia {{ $course->start }} no local {{ $course->local }}</p>
</div>
@endsection
