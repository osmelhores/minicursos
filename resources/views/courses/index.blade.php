@extends('layouts.app')

@section('content')
<div class="container">
  <h1>Minicursos Disponíveis</h1>
  @include('courses.partials.header-links')
  <div class="row justify-content-center">
  <div class="col-md-8">
    @include('courses.partials.list')
  </div>
  </div>
</div>
@endsection
