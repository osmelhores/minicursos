@extends('layouts.app')

@section('content')
<div class="container">
  <h1>Presença do minicurso</h1>
  @include('admin.courses.partials.header-links')
  <table class="table table-bordered table-striped">
    <thead>
      <th>Nome</th>
      <th>Email</th>
      <th>Presente?</th>
    </thead>
    <tbody>
      @foreach($users as $user)
        <tr>
          <td>{{ $user->name }}</a></td>
          <td>{{ $user->email }}</td>
          <td width="1%" nowrap>
            {{ '#' }}
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>

  @if($users->count())
    <form action="{{ route('admin.certificate.generate') }}" method="POST">
      @csrf
      <input type="hidden" name="id" value="{{ $course->id }}">
      <button type="submit" class="btn btn-primary">Gerar Certificados</button>
    </form>
  @endif
</div>
@endsection
