@csrf

<div class="form-group">
  <label for="name">Nome</label>
  <input type="text" id="name" class="form-control" name="name" value="{{ $course->name ?? '' }}">
</div>

<div class="form-group">
  <label for="description">Descrição</label>
  <textarea id="description" class="form-control" name="description" rows="3">{{ $course->description ?? ''}}</textarea>
</div>

<div class="form-group">
  <label for="speaker">Palestrante</label>
  <input type="text" id="speaker" class="form-control" name="speaker" value="{{ $course->speaker ?? '' }}">
</div>

<div class="form-group">
  <label for="local">Local</label>
  <input type="text" id="local" class="form-control" name="local" value="{{ $course->local ?? '' }}">
</div>

<div class="form-group">
  <label for="date">Data</label>
  <input type="date" id="date" class="form-control" name="date" value="{{ $date ?? '' }}">
</div>

<div class="form-group">
  <label for="start">Horário de inicio</label>
  <input type="time" id="start" class="form-control" name="start" value="{{ $start ?? '' }}">
</div>

<div class="form-group">
  <label for="end">Horário de término</label>
  <input type="time" id="end" class="form-control" name="end" value="{{ $end ?? '' }}">
</div>

<div class="form-group">
  <label for="max">Quantidade maxima de alunos</label>
  <input type="number" id="max" class="form-control" name="max" value="{{ $course->max ?? '' }}">
</div>

<div class="form-group">
  <label for="image">Imagem</label>
  <input type="file" id="image" class="form-control" name="image" value="{{ $course->image ?? '' }}">
</div>

<button type="submit" class="btn btn-primary">Salvar</button>
