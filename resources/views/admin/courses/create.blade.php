@extends('layouts.app')

@section('content')
<div class="container">
  <h1>Novo minicurso</h1>
  @include('admin.courses.partials.header-links')
  <form action="{{ route('admin.courses.store') }}" method="POST" class="form" enctype="multipart/form-data">
    @include('admin.courses.partials.form')
  </form>
</div>
@endsection
