@extends('layouts.app')

@section('content')
<div class="container">
  <h1>Minicursos anteriores</h1>
  @include('admin.courses.partials.header-links')
  <table class="table table-bordered table-striped">
    <thead>
      <th>id</th>
      <th>Nome</th>
      <th>opções</th>
    </thead>
    <tbody>
      @foreach($courses as $course)
        <tr>
          <td>{{ $course->id }}</td>
          <td>
            <a href="{{ route('admin.courses.show', $course->id) }}">{{ $course->name }}</a>
          </td>
          <td width="1%" nowrap>
            <a href="{{ route('admin.presences', $course->id) }}" class="btn btn-sm btn-primary">Presenças</a>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
@endsection
