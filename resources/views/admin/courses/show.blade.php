@extends('layouts.app')

@section('content')
<div class="container">
  @include('admin.courses.partials.header-links')
  <h1>{{ $course->name }}</h1>
  <img style="height: 200px; width: 200px; border: solid; float: left;" src="{{ url('storage/courses/'.$course->image) }}" alt="{{ $course->name }}">
  <p>Horário: {{ $course->start }} - {{ $course->end }}</p>
  <p>Local: {{ $course->local }}</p>
  <p>Palestrante: {{ $course->speaker }}</p>
  <p>{{ $course->description }}</p>
  <p>Quantidade de estudantes cadastrados: {{ $course->users()->count() }}</p>

  <a href="{{ route('admin.courses.edit', $course->id) }}" class="btn btn-warning">Editar</a>
</div>
@endsection
