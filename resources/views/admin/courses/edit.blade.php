@extends('layouts.app')

@section('content')
<div class="container">
  <h1>Editar minicurso</h1>
  @include('admin.courses.partials.header-links')
  <form action="{{ route('admin.courses.update', $course->id) }}" method="POST" class="form" enctype="multipart/form-data">
    @method('PUT')
    @include('admin.courses.partials.form')
  </form>
</div>
@endsection
