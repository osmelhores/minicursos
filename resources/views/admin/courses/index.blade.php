@extends('layouts.app')

@section('content')
<div class="container">
  <h1>Área Administrativa</h1>
  <a href="{{ route('admin.courses.create') }}" class="btn btn-primary">Novo minicurso</a>
  <a href="{{ route('admin.courses.previous') }}" class="btn btn-primary">Minicursos anteriores</a>
  <table class="table table-bordered table-striped">
    <thead>
      <th>id</th>
      <th>Nome</th>
      <th>Descrição</th>
      <th>opções</th>
    </thead>
    <tbody>
      @foreach($courses as $course)
        <tr>
          <td>{{ $course->id }}</td>
          <td>{{ $course->name }}</td>
          <td>{{ $course->description }}</td>
          <td width="1%" nowrap>
            <a href="{{ route('admin.courses.show', $course->id) }}" class="btn btn-sm btn-primary">Visualizar</a>
            <a href="{{ route('admin.courses.edit', $course->id) }}" class="btn btn-sm btn-warning">Editar</a>
            <form action="{{ route('admin.courses.destroy', $course->id) }}" method="POST">
              @method('DELETE')
              @csrf
              <button type="submit" class="btn btn-sm btn-danger">Remover</button>
            </form>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
@endsection
