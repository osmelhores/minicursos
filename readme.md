# minicursos

Sistema Web de Controle de Inscrições de Alunos em Minicursos com PHP e MySQL

## Dependencias

- php >= 7.1
- mysql 5.7
- composer

## Clonando o projeto

```bash
git clone https://gitlab.com/osmelhores/minicursos.git
cd minicursos
```

ps: o nome do grupo é péssimo, mas fazer o que?

## Como usar

- Preparando a configuração

copie o arquivo `.env.example` para `.env` e edite o arquivo com os parâmetros necessários

```
DB_CONNECTION=pgsql
DB_HOST=postgres
DB_PORT=5432
DB_DATABASE=homestead   # banco
DB_USERNAME=homestead   # usuário
DB_PASSWORD=secret      # senha
```

- Subindo o projeto

```bash
docker-compose up -d
```

- Instalação das depêndencias

```bash
docker-compose exec app composer install
```

- Gerando a chave do projeto

```bash
docker-compose exec app php artisan key:generate
```
- Migração do banco de dados

```bash
docker-compose exec app php artisan migrate
```

- Populando o banco de dados (optativo)

```bash
docker-compose exec app php artisan db:seed
```

- Execute o projeto

teste no navegador em `http://localhost`.
