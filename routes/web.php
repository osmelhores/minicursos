<?php

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index');

Auth::routes();

Route::middleware('auth')->group(function () {
    Route::middleware('can:normal')->group(function () {
        Route::get('/minhas-inscricoes', 'CourseController@subscribed')->name('courses.subscribed');
        Route::post('/nova-inscricao', 'CourseController@subscribe')->name('courses.subscribe');
        Route::post('/cancelar-inscricao', 'CourseController@unsubscribe')->name('courses.unsubscribe');
        Route::get('/certificados/{user_id}', 'CertificateController@allCertificates')->name('courses.certificates');
        Route::post('/certificado', 'CertificateController@view')->name('courses.certificate');
    });

    Route::middleware('can:admin')->group(function () {
        Route::prefix('/admin')->group(function () {
            Route::get('/', 'AdminController@index')->name('admin.index');

            Route::get('/minicursos', 'AdminController@index')->name('admin.courses.index');
            Route::get('/minicurso/{id}', 'AdminController@show')->name('admin.courses.show');
            Route::get('/cadastrar-minicurso', 'AdminController@create')->name('admin.courses.create');
            Route::post('/cadastrar-minicurso', 'AdminController@store')->name('admin.courses.store');
            Route::get('/editar-minicurso/{id}', 'AdminController@edit')->name('admin.courses.edit');
            Route::put('/editar-minicurso/{id}', 'AdminController@update')->name('admin.courses.update');
            Route::delete('/deletar-minicurso/{id}', 'AdminController@destroy')->name('admin.courses.destroy');
            Route::get('/minicursos-anteriores', 'AdminController@previousCourses')->name('admin.courses.previous');

            Route::get('/presencas/{id}', 'CertificateController@show')->name('admin.presences');
            Route::post('/gerar-certificados', 'CertificateController@generateCertificates')->name('admin.certificate.generate');
        });
    });
});

Route::prefix('/minicursos')->group(function () {
    Route::get('/', 'CourseController@index')->name('courses.index');
    Route::get('/{id}', 'CourseController@show')->name('courses.show');
});
