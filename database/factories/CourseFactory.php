<?php

use Carbon\Carbon;
use Faker\Generator as Faker;
use Minicursos\Course;

$factory->define(Course::class, function (Faker $faker) {
    $start = $faker->dateTimeBetween('-1 week', '1 week', null);

    return [
        'name' => $faker->word,
        'description' => $faker->realText(),
        'speaker' => $faker->name(),
        'start' => $start,
        'end' => $start,
        'local' => str_random(10),
        'max' => rand(5, 10),
    ];
});
