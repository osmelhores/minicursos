<?php

use Illuminate\Database\Seeder;
use Minicursos\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Default Admin
        User::create([
            'name' => 'Admin',
            'email' => 'admin@example.com',
            'password' => password_hash('123456', PASSWORD_BCRYPT),
            'admin' => true,
        ]);

        // Default Students
        array_map(function (int $number) {
            return User::create([
                'name' => sprintf('Student%d', $number),
                'email' => sprintf('student%d@example.com', $number),
                'password' => password_hash('123456', PASSWORD_BCRYPT),
            ]);
        }, [1, 2, 3, 4]);

        factory(User::class, 40)->create();
    }
}
