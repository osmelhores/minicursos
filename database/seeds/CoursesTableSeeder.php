<?php

use Illuminate\Database\Seeder;
use Minicursos\Course;
use Minicursos\User;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Course::class, 10)->create();

        User::all()->map(function (User $user) {
            if ($user->id == 1) {
                return null;
            }
            return $user->courses()->attach(rand(1, Course::all()->count()));
        });
    }
}
